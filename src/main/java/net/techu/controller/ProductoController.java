package net.techu.controller;

import net.techu.data.ProductoMongo;
import net.techu.data.ProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class ProductoController {

    @Autowired
    private ProductoRepository repository;;

    @GetMapping("")
    public String root() {
        return "Techu API REST v1.0.0";
    }

    // Consultar todos los productos
    @GetMapping(value = "/productos", produces="application/json")
    public ResponseEntity<List<ProductoMongo>> getProductos() {
        List<ProductoMongo> lista = repository.findAll();
        return new ResponseEntity<List<ProductoMongo>>(lista, HttpStatus.OK);
    }

    /* Agregar nuevo producto */
    @PostMapping(value = "/productos", produces="application/json")
    public ResponseEntity<String> addProducto(@RequestBody ProductoMongo productoMongo) {

        ProductoMongo resultado=repository.insert(productoMongo);
        return new ResponseEntity<String>(resultado.toString(), HttpStatus.OK);
    }

    // Modificar producto
    @PutMapping("/productos/{id}")
    public ResponseEntity updateProducto(@PathVariable String id, @RequestBody ProductoMongo productoMongo) {

        Optional<ProductoMongo> resultado=repository.findById(id);
        if(resultado.isPresent()){
            resultado.get().descripcion=productoMongo.descripcion;
            resultado.get().precio=productoMongo.precio;
            //resultado.get().users=productoMongo.users;
        }
        ProductoMongo guardado=repository.save(resultado.get());
        return new ResponseEntity<String>(resultado.toString(), HttpStatus.OK);
    }

    //Eliminar Producto
    @DeleteMapping(value="/productos/{id}")
    public ResponseEntity<String> deleteProducto(@PathVariable String id)
    {
        repository.deleteById(id);
        return new ResponseEntity<String>("Producto borrado", HttpStatus.OK);
    }
}
