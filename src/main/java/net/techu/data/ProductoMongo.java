package net.techu.data;

import net.techu.models.UserModel;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document("ProductosSandraPatriciaLabs")
public class ProductoMongo {
    public long id;
    public String descripcion;
    public double precio;
    //public List<UserModel> users;

    public ProductoMongo() {
    }

    public ProductoMongo(long id, String descripcion,double precio, List<UserModel> users) {
        this.id = id;
        this.descripcion = descripcion;
        this.precio = precio;
        //this.users = users;
    }

    @Override
    public String toString() {
        return String.format("Producto [id=%s, descripcion=%s, precio=%s]", id, descripcion, precio);
    }
}
