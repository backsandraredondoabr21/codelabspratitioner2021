package com.example.LabPractitioner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//@SpringBootApplication
public class LabPractitionerApplication {

	public static void main(String[] args) {
		SpringApplication.run(LabPractitionerApplication.class, args);
	}

}
